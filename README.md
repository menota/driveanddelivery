# 🚚 DriveAndDeliver - a Carrefour kata

## Installation
- maven
- Java 21
- Git
- postgres

## Bootstrap

- ./mvnw spring-boot:run

## Features
    - choose Delivery Method
    - Choose Time slot for a specific  delivery method and book 

### Bonus Features

#### REST API

- Secure the API: I suggest to develop an authserver for authorization and authentication 
- for the archetictural style, i suggest to implement a system based on microservices and an hexagonal architecture and implement event driven design and the saga pattern.


#### Persistence
- I used spring data jpa with postgres to persist data, I need to enhance relations between tables an the transactionnal aspect for read and update operations 
- cache solution : I suggest to use Redis to store sessions 

#### Stream
- we can utilize Apache Kafka  To implement a data streaming solution
-  we can use spring-kafka to send  and receive messages 

### CI/CD
- since our project is on gitlab we can set up CI/CD pipeline using Gitlab CI/CD and kubernetes 
- For tests, we should start cover all our application we unit tests then proced to use tools like selenium for end to end tests 

### Packaging
- Create a container of your application 
- Deploy your application in a pod
- Create a native image of your application
