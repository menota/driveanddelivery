package com.nebtimehdi.driveanddeliver;

import com.nebtimehdi.driveanddeliver.services.TimeSlotService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DriveAndDeliveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DriveAndDeliveryApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(TimeSlotService timeSlotService){
		return args -> {
			timeSlotService.generateRandomTimeSlots(25);
		};
	}
}
