package com.nebtimehdi.driveanddeliver.services;

import com.nebtimehdi.driveanddeliver.models.DeliveryMethod;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DeliveryMethodService implements IDeliveryMethodService {
    @Override
    public List<DeliveryMethod> getAll() {
        return List.of(DeliveryMethod.values());
    }
}
