package com.nebtimehdi.driveanddeliver.services;

import com.nebtimehdi.driveanddeliver.models.DeliveryMethod;
import com.nebtimehdi.driveanddeliver.models.TimeSlot;
import com.nebtimehdi.driveanddeliver.repositories.TimeSlotRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@AllArgsConstructor
public class TimeSlotService implements   ITimeSlotService{

    private TimeSlotRepository timeSlotRepository;

    @Override
    public List<TimeSlot> getTimeSlotsForDeliveryType(DeliveryMethod deliveryMethod) {

        return timeSlotRepository.findByDeliveryMethod(deliveryMethod);
    }

    @Override
    public Optional<TimeSlot> findById(Integer timeSlotId) {

        return timeSlotRepository.findById(timeSlotId);
    }

    @Override
    public TimeSlot save(TimeSlot timeSlot) {

        return timeSlotRepository.save(timeSlot);
    }

    public static TimeSlot generateRandomTimeSlot() {
        Random random = new Random();
        long offset = Timestamp.valueOf("2024-01-01 00:00:00").getTime();
        long end = Timestamp.valueOf("2025-01-01 00:00:00").getTime();
        long diff = end - offset + 1;
        Timestamp startTime = new Timestamp(offset + (long)(Math.random() * diff));
        Timestamp endTime = new Timestamp(startTime.getTime() + (long)(Math.random() * 3600 * 1000)); // Adding up to 1 hour
        boolean available = random.nextBoolean();
        DeliveryMethod deliveryMethod = DeliveryMethod.values()[random.nextInt(DeliveryMethod.values().length)];

        return TimeSlot.builder().startTime(startTime)
                .endTime(endTime)
                .deliveryMethod(deliveryMethod)
                .available(available)
                .build();
    }
    public  List<TimeSlot> generateRandomTimeSlots(int count) {
        List<TimeSlot> timeSlots = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            timeSlotRepository.save(generateRandomTimeSlot());
        }
        return timeSlots;
    }
}
