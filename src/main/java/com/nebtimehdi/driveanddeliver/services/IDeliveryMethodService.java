package com.nebtimehdi.driveanddeliver.services;

import com.nebtimehdi.driveanddeliver.models.DeliveryMethod;

import java.util.List;

public interface IDeliveryMethodService {

     List<DeliveryMethod> getAll();
}
