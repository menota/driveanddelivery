package com.nebtimehdi.driveanddeliver.services;

import com.nebtimehdi.driveanddeliver.models.Booking;

import java.util.List;
import java.util.Optional;

public interface IBookingService {


    Booking save(Booking booking);

    Optional<Booking> findOne(Integer bookingId);

    List<Booking> findAll();
}
