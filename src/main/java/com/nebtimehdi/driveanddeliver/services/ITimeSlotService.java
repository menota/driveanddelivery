package com.nebtimehdi.driveanddeliver.services;

import com.nebtimehdi.driveanddeliver.models.DeliveryMethod;
import com.nebtimehdi.driveanddeliver.models.TimeSlot;

import java.util.List;
import java.util.Optional;

public interface ITimeSlotService {

    List<TimeSlot> getTimeSlotsForDeliveryType(DeliveryMethod deliveryMethod);

    Optional<TimeSlot> findById(Integer timeSlotId);
    TimeSlot save(TimeSlot timeSlot);

}
