package com.nebtimehdi.driveanddeliver.services;

import com.nebtimehdi.driveanddeliver.models.Booking;
import com.nebtimehdi.driveanddeliver.repositories.BookingRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BookingService implements  IBookingService{
    private final BookingRepository bookingRepository;

    @Override
    public Booking save(Booking booking) {

        return    bookingRepository.save(booking);
    }

    @Override
    public Optional<Booking> findOne(Integer bookingId) {
        return bookingRepository.findById(bookingId);    }

    @Override
    public List<Booking> findAll() {
        return bookingRepository.findAll();
    }
}
