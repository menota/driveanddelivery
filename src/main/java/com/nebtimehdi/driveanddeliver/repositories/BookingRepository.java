package com.nebtimehdi.driveanddeliver.repositories;

import com.nebtimehdi.driveanddeliver.models.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends JpaRepository<Booking,Integer> {
}
