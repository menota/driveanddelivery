package com.nebtimehdi.driveanddeliver.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Booking {

    @Id
    @GeneratedValue
    private Integer id;

    private int customerId;

    private int orderId;

    private int timeSlotId;
    @Enumerated(EnumType.STRING)
    private DeliveryMethod deliveryMethod;

}
