package com.nebtimehdi.driveanddeliver.models;

public enum DeliveryMethod {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
