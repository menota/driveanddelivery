package com.nebtimehdi.driveanddeliver.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class TimeSlot {
    @Id
    @GeneratedValue
    private Integer id ;

    private Timestamp startTime ;

    private Timestamp endTime ;

    private boolean available ;

    @Enumerated(EnumType.STRING)
    private DeliveryMethod deliveryMethod ;
}