package com.nebtimehdi.driveanddeliver.controllers;

import com.nebtimehdi.driveanddeliver.exceptions.BookingNotFoundException;
import com.nebtimehdi.driveanddeliver.exceptions.TimeSlotNotFoundException;
import com.nebtimehdi.driveanddeliver.models.Booking;
import com.nebtimehdi.driveanddeliver.models.TimeSlot;
import com.nebtimehdi.driveanddeliver.services.IBookingService;
import com.nebtimehdi.driveanddeliver.services.ITimeSlotService;
import com.nebtimehdi.driveanddeliver.services.TimeSlotService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/booking")
@AllArgsConstructor
public class BookingController {

    private final IBookingService bookingService;
    private final ITimeSlotService timeSlotService;

    @PostMapping
    public ResponseEntity<Booking> createBooking( @RequestBody Booking booking){

        return new ResponseEntity<>(bookingService.save(booking), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Booking> updateBooking(@PathVariable Integer id, @RequestBody Booking booking){

        var timeSlot= timeSlotService.findById(booking.getTimeSlotId()).orElseThrow(TimeSlotNotFoundException::new);
        timeSlot.setAvailable(false);
        timeSlotService.save(timeSlot);
        var bookingToUpdate = bookingService.findOne(id).orElseThrow(BookingNotFoundException::new);
        bookingToUpdate.setTimeSlotId(booking.getTimeSlotId());

        return ResponseEntity.ok().body( bookingService.save(bookingToUpdate));
    }

    @GetMapping("/booking")
    public ResponseEntity<List<Booking>> findAllBookingList(){

        return ResponseEntity.ok().body(bookingService.findAll());
    }

}
