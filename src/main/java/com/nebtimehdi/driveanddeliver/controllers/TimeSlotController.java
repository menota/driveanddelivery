package com.nebtimehdi.driveanddeliver.controllers;

import com.nebtimehdi.driveanddeliver.models.DeliveryMethod;
import com.nebtimehdi.driveanddeliver.models.TimeSlot;
import com.nebtimehdi.driveanddeliver.services.ITimeSlotService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/timeslots")
@AllArgsConstructor
public class TimeSlotController {

    private final ITimeSlotService timeSlotService ;


    @GetMapping("/{deliveryMethod}")
    public ResponseEntity<List<TimeSlot>> findTimeSlots(@PathVariable DeliveryMethod deliveryMethod){


        return new ResponseEntity<>(timeSlotService.getTimeSlotsForDeliveryType(deliveryMethod), HttpStatus.OK);

    }
}
