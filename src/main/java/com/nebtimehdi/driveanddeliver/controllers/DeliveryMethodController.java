package com.nebtimehdi.driveanddeliver.controllers;

import com.nebtimehdi.driveanddeliver.models.DeliveryMethod;
import com.nebtimehdi.driveanddeliver.services.DeliveryMethodService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/deliverymethod")
@AllArgsConstructor
public class DeliveryMethodController {

    private final DeliveryMethodService deliveryMethodService;


    @GetMapping
    public ResponseEntity<List<DeliveryMethod>> getAllDeliveryMethod(){

        return new ResponseEntity<>(deliveryMethodService.getAll(), HttpStatus.OK);
    }
}
